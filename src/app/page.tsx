/* eslint-disable @next/next/no-img-element */
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import Image from 'next/image';

import { Logo } from '~/assets/logo';
import { navigation } from '~/constants/menu';

dayjs.extend(relativeTime);

const pets = [
  {
    thumbnail: '/assets/pets/1.png',
    code: 'MO231',
    title: 'Pomeranian White',
    sex: 'male',
    birth_at: '2024-01-02T08:10:20',
    price: 6_900_000,
  },
  {
    thumbnail: '/assets/pets/2.png',
    code: 'MO502',
    title: 'Poodle Tiny Yellow',
    sex: 'female',
    birth_at: '2024-01-20T08:10:20',
    price: 3_900_000,
  },
  {
    thumbnail: '/assets/pets/3.png',
    code: 'MO102',
    title: 'Poodle Tiny Sepia',
    sex: 'male',
    birth_at: '2024-01-20T08:10:20',
    price: 4_000_000,
  },
  {
    thumbnail: '/assets/pets/4.png',
    code: 'MO512',
    title: 'Alaskan Malamute Grey',
    sex: 'male',
    birth_at: '2024-01-20T08:10:20',
    price: 6_900_000,
  },
];

const sellers = [
  {
    thumbnail: '/assets/sellers/1.png',
    title: 'Sheba',
  },
  {
    thumbnail: '/assets/sellers/2.png',
    title: 'Whiskas',
  },
  {
    thumbnail: '/assets/sellers/3.png',
    title: 'Bakers',
  },
  {
    thumbnail: '/assets/sellers/4.png',
    title: 'Felix',
  },
  {
    thumbnail: '/assets/sellers/5.png',
    title: 'Good Boy',
  },
  {
    thumbnail: '/assets/sellers/6.png',
    title: 'Butchers',
  },
  {
    thumbnail: '/assets/sellers/7.png',
    title: 'Pedigree',
  },
];

export default function Home() {
  // const [mobileMenuOpen, setMobileMenuOpen] = useState(false)

  // const toggleMenu = (value: boolean) => {
  //   // setMobileMenuOpen(true)
  // };

  return (
    <main className="relative">
      <img
        src="/assets/banners/1.png"
        alt=""
        className="absolute top-0 left-0 w-full"
      />
      <header className="container mx-auto absolute inset-x-0 top-0 z-50">
        <nav
          className="flex items-center justify-between p-6 lg:px-8"
          aria-label="Global"
        >
          <div className="relative flex lg:flex-1">
            <a href="#">
              <span className="sr-only">Your Company</span>
              <Logo />
            </a>
          </div>
          <div className="flex lg:hidden">
            <button
              type="button"
              className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
              // onClick={() => toggleMenu(true)}
            >
              <span className="sr-only">Open main menu</span>
              {/* <Bars3Icon className="h-6 w-6" aria-hidden="true" /> */}
            </button>
          </div>
          <div className="hidden lg:flex lg:gap-x-12">
            {navigation.map(item => (
              <a
                key={item.name}
                href={item.href}
                className="text-sm font-semibold leading-6 text-gray-900"
              >
                {item.name}
              </a>
            ))}
          </div>
          <div className="hidden lg:flex lg:flex-1 lg:justify-end gap-4">
            <input
              type="text"
              name="search"
              id="search"
              placeholder="Search something here!"
              className="rounded-full py-2 px-5"
            />
            <a
              className="inline-block rounded-full bg-gray-800 px-12 py-3 text-sm text-white focus:outline-none focus:ring"
              href="#"
            >
              Join the community
            </a>
            <button type="button">VND</button>
          </div>
        </nav>
        {/* <Dialog as="div" className="lg:hidden" open={mobileMenuOpen} onClose={setMobileMenuOpen}>
          <div className="fixed inset-0 z-50" />
          <Dialog.Panel className="fixed inset-y-0 right-0 z-50 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
            <div className="flex items-center justify-between">
              <a href="#" className="-m-1.5 p-1.5">
                <span className="sr-only">Your Company</span>
                <img
                  className="h-8 w-auto"
                  src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                  alt=""
                />
              </a>
              <button
                type="button"
                className="-m-2.5 rounded-md p-2.5 text-gray-700"
                onClick={() => setMobileMenuOpen(false)}
              >
                <span className="sr-only">Close menu</span>
                <XMarkIcon className="h-6 w-6" aria-hidden="true" />
              </button>
            </div>
            <div className="mt-6 flow-root">
              <div className="-my-6 divide-y divide-gray-500/10">
                <div className="space-y-2 py-6">
                  {navigation.map((item) => (
                    <a
                      key={item.name}
                      href={item.href}
                      className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                    >
                      {item.name}
                    </a>
                  ))}
                </div>
                <div className="py-6">
                  <a
                    href="#"
                    className="-mx-3 block rounded-lg px-3 py-2.5 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                  >
                    Log in
                  </a>
                </div>
              </div>
            </div>
          </Dialog.Panel>
        </Dialog> */}
      </header>

      <section
        id="hero"
        className="relative isolate container mx-auto mb-8 pt-14 lg:px-8"
      >
        <div className="max-w-2xl py-32 sm:py-48 lg:py-56">
          <div>
            <h1 className="text-6xl font-bold tracking-tight text-gray-900 mb-4">
              One more friend
            </h1>
            <h2 className="text-5xl font-bold tracking-tight text-gray-900">
              Thousands more fun!
            </h2>
            <p className="mt-6 text-lg leading-8 text-gray-600">
              Having a pet means you have more joy, a new friend, a happy person
              who will always be with you to have fun. We have 200+ different
              pets that can meet your needs!
            </p>
            <div className="mt-6 flex items-center gap-x-6">
              <a
                className="inline-block rounded-full border-2 border-blue-900 px-12 py-3 text-sm text-gray-800 focus:outline-none focus:ring"
                href="#"
              >
                View Intro
              </a>
              <a
                className="inline-block rounded-full bg-gray-800 px-12 py-3 text-sm text-white focus:outline-none focus:ring"
                href="#"
              >
                Explore Now
              </a>
            </div>
          </div>
        </div>
      </section>

      <div className="container mx-auto">
        <section id="latest-pets">
          <div className="grid grid-cols-2 gap-2 mb-4">
            <div>
              <h3>Whats new?</h3>
              <h2 className="font-semibold text-xl">
                Take a look at some of our pets
              </h2>
            </div>
            <div className="text-end">
              <a
                className="inline-block rounded-full border-2 border-blue-900 px-8 py-2 text-sm text-gray-800 focus:outline-none focus:ring"
                href="#"
              >
                View More
              </a>
            </div>
          </div>
          <div className="grid grid-cols-4 gap-4">
            {[...pets, ...pets].map((pet, idx) => (
              <div key={idx} className="card">
                <div className="w-[264px] h-[264px] relative mb-4">
                  <Image src={pet.thumbnail} fill alt={pet.title} />
                </div>
                <h3 className="font-semibold">
                  {pet.code} - {pet.title}
                </h3>
                <div className="mb-1 text-[12px]">
                  Gene: <span className="font-medium">{pet.sex}</span> · Age:{' '}
                  <span className="font-medium">
                    {dayjs(pet.birth_at).fromNow(true)}
                  </span>
                </div>
                <div className="font-semibold text-sm">
                  {pet.price.toLocaleString('id-ID')} VND
                </div>
              </div>
            ))}
          </div>
        </section>

        <section id="banner-2" className="relative mb-8">
          <img src="/assets/banners/2.png" alt="" className="w-full" />
          <div className="max-w-2xl py-32 sm:py-48 lg:py-56 absolute top-0 right-0">
            <div className="text-right">
              <h1 className="text-5xl font-bold tracking-tight text-gray-900 mb-4">
                One more friend
              </h1>
              <h2 className="text-4xl font-bold tracking-tight text-gray-900">
                Thousands more fun!
              </h2>
              <p className="mt-6 text-lg leading-8 text-gray-600">
                Having a pet means you have more joy, a new friend, a happy
                person who will always be with you to have fun. We have 200+
                different pets that can meet your needs!
              </p>
              <div className="mt-6 flex items-center justify-end gap-x-6">
                <a
                  className="inline-block rounded-full border-2 border-blue-900 px-12 py-3 text-sm text-gray-800 focus:outline-none focus:ring"
                  href="#"
                >
                  View Intro
                </a>
                <a
                  className="inline-block rounded-full bg-gray-800 px-12 py-3 text-sm text-white focus:outline-none focus:ring"
                  href="#"
                >
                  Explore Now
                </a>
              </div>
            </div>
          </div>
        </section>

        <section id="products">{/* TODO */}</section>

        <section id="sellers">
          <h3>Proud to be part of Pet Sellers</h3>
          <div className="grid grid-cols-7 gap-2 h-[112px]">
            {sellers.map(seller => (
              <div key={seller.title} className="relative">
                <Image
                  src={seller.thumbnail}
                  fill
                  style={{
                    objectFit: 'contain',
                  }}
                  alt={seller.title}
                />
              </div>
            ))}
          </div>
        </section>

        <section id="banner-3" className="mb-8">
          <img src="/assets/banners/3.png" alt="" className="w-full" />
        </section>

        <section id="blog">{/* TODO */}</section>
      </div>
    </main>
  );
}
