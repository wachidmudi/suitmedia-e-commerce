import type { Metadata } from 'next';
import localFont from 'next/font/local';
import { Footer } from '~/components/footer';
import './globals.css';

const svnGilroy = localFont({
  src: [
    {
      path: '../assets/fonts/svn-gilroy-light.woff',
      weight: '500',
      style: 'normal',
    },
    {
      path: '../assets/fonts/svn-gilroy-bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
});

export const metadata: Metadata = {
  title: 'Monito',
  description: 'Online Pet Shop',
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={svnGilroy.className}>
        {children}
        <Footer />
      </body>
    </html>
  );
}
