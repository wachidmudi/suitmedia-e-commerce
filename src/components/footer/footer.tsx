/* eslint-disable @next/next/no-img-element */
import { Logo } from '~/assets/logo';
import { navigation } from '~/constants/menu';

export function Footer() {
  return (
    <footer className="bg-[#FCEED5] rounded-t-3xl">
      <div className="container mx-auto py-16">
        <div className="bg-[#003459] rounded-2xl h-[136px] p-6">
          <div className="grid grid-cols-2 gap-4">
            <div className="text-[#FDFDFD] text-2xl font-semibold">
              Register now so you don&apos;t miss our programs
            </div>
            <div>
              <form className="flex gap-4 bg-white rounded-xl p-3">
                <div className="grow">
                  <input
                    className="rounded-lg border border-gray-400 py-2 px-6 w-full"
                    type="email"
                    name="email"
                    id="email"
                    placeholder="Enter your email"
                    autoComplete="email"
                  />
                </div>
                <div className="flex-none">
                  <button
                    className="bg-[#003459] rounded-lg text-white py-2 px-6"
                    type="submit"
                  >
                    Subscribe Now
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div className="grid grid-cols-2 my-8">
          <div>
            <nav className="flex gap-12">
              {navigation.map(item => (
                <a
                  key={item.name}
                  href={item.href}
                  className="leading-6 text-gray-900"
                >
                  {item.name}
                </a>
              ))}
            </nav>
          </div>
          <ul className="list-none m-0 flex flex-row-reverse gap-12">
            <li>
              <a href="#" className="leading-6 text-gray-900">
                <img
                  src="/assets/icons/facebook.png"
                  width={24}
                  height={24}
                  alt="Facebook"
                />
              </a>
            </li>
            <li>
              <a href="#" className="leading-6 text-gray-900">
                <img
                  src="/assets/icons/twitter.png"
                  width={24}
                  height={24}
                  alt="Twitter"
                />
              </a>
            </li>
            <li>
              <a href="#" className="leading-6 text-gray-900">
                <img
                  src="/assets/icons/instagram.png"
                  width={24}
                  height={24}
                  alt="Instagram"
                />
              </a>
            </li>
            <li>
              <a href="#" className="leading-6 text-gray-900">
                <img
                  src="/assets/icons/youtube.png"
                  width={24}
                  height={24}
                  alt="Youtube"
                />
              </a>
            </li>
          </ul>
        </div>

        <hr className="my-12 bg-slate-600 h-[0.5px]" />

        <div className="grid grid-cols-3 items-center">
          <div>© 2022 Monito. All rights reserved.</div>
          <div className="flex justify-center">
            <Logo />
          </div>
          <div className="text-right">
            <ul className="list-none m-0 flex flex-row-reverse gap-12">
              <li>
                <a href="#" className="leading-6 text-gray-900">
                  Terms of Service
                </a>
              </li>
              <li>
                <a href="#" className="leading-6 text-gray-900">
                  Privacy Policy
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
}
